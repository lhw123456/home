//
//  Storage.swift
//  home
//
//  Created by Yun Zeng on 2019/8/4.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import Foundation

class Storage {
    static let Path: URL = {
#if os(iOS)
        return try! FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
#else
        var path = "file://"
        for component in #file.split(separator: "/") {
            if component == "Model" {
                break
            }
            path = path + "/" + component
        }
        return URL(string: path + "/Doc")!
#endif
    } ()
}

