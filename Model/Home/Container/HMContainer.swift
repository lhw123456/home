//
//  HMContainer.swift
//  home
//
//  Created by Yun Zeng on 2018/11/25.
//  Copyright © 2018 Yun Zeng. All rights reserved.
//

import Foundation

//
// Container 设备容器
// 特指一个独立的wifi设备，能桥接不能通过wifi联网的子设备，如zigbee子设备
// 当一个容器离线时，从属的子设备也会全部离线
//
class HMContainer: Codable {
    var id: String
    var version: Int = 0
    var reachable: Bool {
        didSet {
            if reachable == false {
                for accessory in self.accessories {
                    accessory.reachable = false
                }
            }
        }
    }
    
    var accessories: [HMAccessory]
    
    private enum CodingKeys: String, CodingKey {
        case id
        case version
        case reachable
        case accessories
    }
    
    init(id: String, version: Int = 0, reachable: Bool, accessories: [HMAccessory]) {
        self.id = id
        self.version = version
        self.reachable = reachable
        self.accessories = accessories
    }
}

// 调试打印
extension HMContainer: CustomStringConvertible {
    var description: String {
        let data = try! JSONEncoder().encode(self)
        return String(data: data, encoding: .utf8)!
    }
}





