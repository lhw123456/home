//
//  HomeAutomationTrigger.swift
//  home
//
//  Created by Yun Zeng on 2019/3/10.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import Foundation

// 自动化触发条件: 定时器
class EventTimer: Codable {
    var minuteOfDay: Int = 0
    var week: UInt8 = 0
    private enum CodingKeys: String, CodingKey {
        case minuteOfDay = "timer"
        case week
    }
}

// 自动化条件之: 服务属性变化
class EventCharacteristic: Codable {
    // 属性变化触发属性条件之: 相等、大于(如湿度大于75%)、小于(温度小于25度)、变化
    enum ThresholdType: Int {
        case equal = 0
        case greater = 1
        case smaller = 2
        case changes = 3
    }
    
    var aid: String
    var sid: Int
    var cid: Int
    var value: AnyJSONType
    
    var thresholdType: Int = ThresholdType.equal.rawValue
    
    private enum CodingKeys: String, CodingKey {
        case aid
        case sid
        case cid
        case value
        case thresholdType = "threshold_t"
    }
    
    init?(characteristic: HMCharacteristic, target: AnyJSONType, type: ThresholdType = .equal) {
        guard let service = characteristic.service else {
            return nil
        }
        guard let accessory = service.accessory else {
            return nil
        }
        
        self.aid = accessory.id
        self.sid = service.id
        self.cid = characteristic.id
        self.value = target
        self.thresholdType = type.rawValue
    }
}

extension EventCharacteristic: CustomStringConvertible {
    var description: String {
        let data = try! JSONEncoder().encode(self)
        return String(data: data, encoding: .utf8)!
    }
}

// 自动化触发条件: 位置变化t触发(例如家里有人回家或者离家)
class EventLocation: Codable {
    enum TriggerType: Int {
        case leave = 0
        case arrive = 1
        case firstArrive = 3
        case lastLeave = 4
    }
    
    var members: [String]
    var triggerType: Int
    
    private enum CodingKeys: String, CodingKey {
        case members
        case triggerType = "trigger_t"
    }
    
    init(members: [String], triggerType: TriggerType) {
        self.members = members
        self.triggerType = triggerType.rawValue
    }
}

// 自动化触发条件解析
extension AutomationTrigger.TriggerType: Codable {
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let timer = try? container.decode(EventTimer.self) {
            self = .timer(timer)
        } else if let location = try? container.decode(EventLocation.self) {
            self = .location(location)
        } else if let characteristic = try? container.decode(EventCharacteristic.self) {
            self = .characteristic(characteristic)
        } else {
            self = .unknown
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case let .timer(timer):
            try container.encode(timer)
        case let .location(location):
            try container.encode(location)
        case let .characteristic(characteristic):
            try container.encode(characteristic)
        case .unknown:
            try container.encodeNil()
        }
    }
}

// 自动化触发条件
class AutomationTrigger: Codable {
    enum TriggerType {
        case timer(EventTimer)
        case location(EventLocation)
        case characteristic(EventCharacteristic)
        case unknown
    }
    
    var type: String
    var trigger: TriggerType
    private enum CodingKeys: String, CodingKey {
        case type
        case trigger = "desc"
    }
    
    init(type: String, trigger: TriggerType) {
        self.type = type
        self.trigger = trigger
    }
}









