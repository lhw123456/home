//
//  Location.swift
//  home
//
//  Created by Yun Zeng on 2019/9/23.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import Foundation

/**
 * 家庭成员位置改变发送消息
 * {
 *   "path" : "home/member/location",
 *   "method": "post",
 *   "home": "187c7894-1d3d-442b-9d3a-d2ebcfa86a6c",
 *   "data": {
 *       "member": "ce173611-1d3d-4414-94ff-1764fc7812f3",
 *       "state": "arrive"
 *   }
 * }
 */

class LocationManager {
    enum State: String, Codable {
        case arrive = "arrive"
        case leave = "leave"
    }
    
    struct MemberLocationInfo: Codable {
        var member: String
        var state: State
    }
    
    static func postMemberState(membership: Membership, home: String, state: State) {
        let info = MemberLocationInfo(member: membership.uuid!, state: state)
        let msg = CloudMessage(path: "home/member/location", method: .post, object: info)
        _ = HttpClient.post(membership: membership, home: home, message: msg) { _ in
        }
    }
}

