//
//  main.swift
//  scratch
//
//  Created by Yun Zeng on 2019/8/4.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import Foundation

Logger.Info("let's go!")

let date = "2019-10-07 12:37:23".toDate()
let str = date.toString(format: "YYYY-MM-dd HH") + ":00:00"

let interval = date.timeIntervalSince(str.toDate())
print("interval: \(interval)")


let components = Calendar.current.dateComponents(in: TimeZone.current, from: date)
print("hour: \(components.hour!)")

print("value = \(12 % 2)")

let hour = components.hour! - (components.hour! % 2)
for i in 0 ... 12 {
    print("\((hour + i * 2) % 24)")
}

