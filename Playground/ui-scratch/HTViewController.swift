//
//  HTViewController.swift
//  home
//
//  Created by Yun Zeng on 2019/9/24.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit
import CoreGraphics


class HTViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(hex: "efefef")
        
        let header = UIView()
        self.view.addSubview(header)
        
        header.translatesAutoresizingMaskIntoConstraints = false
        header.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        header.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        header.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        header.heightAnchor.constraint(equalToConstant: 100).isActive = true
        header.backgroundColor = UIColor.white
        
        let tChartView = HTChartView(title: "25.7", type: .temperature)
        tChartView.addToSuperView(view: self.view, topAnchor: header.bottomAnchor)
        
        if let data = CharacteristicStatistics(data: TemperatureData, type: .temperature) {
            tChartView.chartView.data = data
        }
        
        let hChartView = HTChartView(title: "69", type: .humidity)
        hChartView.addToSuperView(view: self.view, topAnchor: tChartView.bottomAnchor)
        
        if let data = CharacteristicStatistics(data: HumidityData, type: .humidity) {
            hChartView.chartView.data = data
        }
        
    }
}


fileprivate enum ChartType {
    case temperature
    case humidity
}

fileprivate class HTChartView: UIView {
    private let themeColor = UIColor(hex: "18A2FF")
    private let textColor = UIColor(hex: "5F5F5F")
    private let lineColor = UIColor(hex: "DEDEDE")
    
    // 标题文字
    private lazy var titleAttributes: [NSAttributedString.Key : Any] =
        [
             NSAttributedString.Key.font: UIFont(name: "PingFangHK-Medium", size: 36)!,
             NSAttributedString.Key.kern: -1,
             NSAttributedString.Key.foregroundColor: self.themeColor
        ]
    
    // 图标
    lazy var chartView: ChartView = {
        let view = ChartView(type: self.chartType)
        view.backgroundColor = UIColor.clear
        return view
    } ()
    
    var title: String
    var chartType = ChartType.temperature
    

    init(title: String, type: ChartType) {
        self.title = title
        self.chartType = type
        
        super.init(frame: .zero)
        self.backgroundColor = UIColor.clear
        
        self.showTitle()
        
        self.addSubview(chartView)
        chartView.translatesAutoresizingMaskIntoConstraints = false
        chartView.topAnchor.constraint(equalTo: self.topAnchor, constant: 70).isActive = true
        chartView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        chartView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        chartView.heightAnchor.constraint(equalToConstant: 260).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addToSuperView(view: UIView, topAnchor: NSLayoutYAxisAnchor) {
        view.addSubview(self)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: topAnchor).isActive = true
        self.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12).isActive = true
        self.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -12).isActive = true
        self.heightAnchor.constraint(equalToConstant: 324).isActive = true
    }
    
    // 标题
    private func showTitle() {
        let label = UILabel()
        label.attributedText = NSAttributedString(string: self.title, attributes: titleAttributes)
        label.autoresizingMask = .flexibleWidth
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        label.topAnchor.constraint(equalTo: self.topAnchor, constant: 18).isActive = true
        
        let tlabel = UILabel()
        self.addSubview(tlabel)
        let attributes: [NSAttributedString.Key : Any] =
            [
                .font: UIFont(name: "PingFangHK-Regular", size: 21)!,
                .kern: -0.49,
                .foregroundColor: textColor
            ]
        var ttext = "温度"
        if self.chartType == .humidity {
            ttext = "湿度"
        }
        tlabel.attributedText = NSAttributedString(string: ttext, attributes: attributes)
        tlabel.translatesAutoresizingMaskIntoConstraints = false
        tlabel.trailingAnchor.constraint(equalTo: label.leadingAnchor, constant: -5).isActive = true
        tlabel.centerYAnchor.constraint(equalTo: label.centerYAnchor, constant: 3).isActive = true
        
        let clabel = UILabel()
        self.addSubview(clabel)
        let cattributes: [NSAttributedString.Key : Any] =
            [
                .font: UIFont(name: "PingFangHK-Medium", size: 21)!,
                .kern: -0.49,
                .foregroundColor: themeColor
            ]
        var ctext = "℃"
        if self.chartType == .humidity {
            ctext = "%"
        }
        clabel.attributedText = NSAttributedString(string: ctext, attributes: cattributes)
        clabel.translatesAutoresizingMaskIntoConstraints = false
        clabel.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: 2.5).isActive = true
        clabel.centerYAnchor.constraint(equalTo: label.centerYAnchor, constant: 5).isActive = true
    }
}

// MARK: - 统计图
fileprivate class ChartView: UIView {
    
    private var ctype: ChartType
    
    init(type: ChartType) {
        self.ctype = type
        super.init(frame: .zero)
        self.layer.cornerRadius = 8
    }
    
    private let titleHeight: CGFloat = 30
    private let axisHeight: CGFloat = 30
    private let vspace: CGFloat = 18
    
    var data: CharacteristicStatistics? {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    private var attributes: [NSAttributedString.Key: Any] =
        [
            .font: UIFont(name: "PingFangSC-Regular", size: 14)!,
            .kern: 0.17,
            .foregroundColor: UIColor(hex: "5F5F5F")
        ]
    
    // 绘制标题
    func drawTitle(rect: CGRect) {
        let path = UIBezierPath(rect: CGRect(x: 0, y: self.titleHeight - 0.5, width: rect.width, height: 0.5))
        UIColor(hex: "D4D4D4").setFill()
        path.fill()
                
        var text: NSString = "10月7日"
        text.draw(at: CGPoint(x: 12, y: 6), withAttributes: attributes)
        
        if let average = self.data?.average {
            if ctype == .temperature {
                text = NSString(format: "平均: %0.1f°", average)
            } else {
                text = NSString(format: "平均: %0.0f%%", average)
            }
        } else {
            text = "平均: --"
        }
        let size = text.boundingRect(with: CGSize(),
                                     options: .usesLineFragmentOrigin,
                                     attributes: attributes, context: nil)
        text.draw(at: CGPoint(x: rect.width - 12 - size.width, y: 6), withAttributes: attributes)
    }
    
    private var tStart = Date()
    
    // 绘制坐标
    func drawAxis(rect: CGRect) {
        // 直线上面的横线
        let path = UIBezierPath(rect: CGRect(x: 0, y: rect.height - self.axisHeight + 0.5, width: rect.width, height: 0.5))
        UIColor(hex: "D4D4D4").setFill()
        path.fill()
        
        // 坐标轴左边的起始竖线
        let lleft = UIBezierPath(rect: CGRect(x: vspace, y: rect.height - self.axisHeight - 3, width: 0.5, height: 7))
        UIColor(hex: "D4D4D4").setFill()
        lleft.fill()
        
        // 坐标轴右边的结束竖线
        let lright = UIBezierPath(rect: CGRect(x: rect.width - vspace, y: rect.height - self.axisHeight - 3, width: 0.5, height: 7))
        UIColor(hex: "D4D4D4").setFill()
        lright.fill()
        
        let interval = self.tStart.timeIntervalSince((tStart.toString(format: "YYYY-MM-dd HH") + ":00:00").toDate())
        let components = Calendar.current.dateComponents(in: TimeZone.current, from: tStart)
        let hour = components.hour!
        
        let radio = CGFloat((3600 - interval) / (24 * 3600))
        let start = vspace + (rect.width - vspace * 2) * radio
        let space: CGFloat = (rect.width - vspace * 2) / 12
        for i in 0 ... 12 {
            let x = (start + CGFloat(i) * space)
            if x > (rect.width - vspace) {
                break
            }
            let y = rect.height - self.axisHeight + 0.5
            // 圆点
            let ovalPath = UIBezierPath(ovalIn: CGRect(x: x  - 1, y: y - 1, width: 2.5, height: 2.5))
            UIColor(hex: "D4D4D4").setFill()
            ovalPath.fill()
            
            // 文字
            let h = (hour + i * 2 + 1) % 24
            if h == 0 {
                attributes[.foregroundColor] = UIColor(hex: "18A2FF")
            } else {
                attributes[.foregroundColor] = UIColor(hex: "5F5F5F")
            }
            
            let text = NSString(format: "%d", h)
            let size = text.boundingRect(with: CGSize(), options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            text.draw(at: CGPoint(x: x - size.width / 2, y: y + 4), withAttributes: attributes)
        }
        
        // 横线
        attributes[.kern] = -0.53
        if self.ctype == .temperature {
            // 16度以下为寒冷区域
            // 16度-26度为舒适区
            // 26度以上为炎热区
            // 温度区间0-40度
            let height = rect.height - self.axisHeight - self.titleHeight
            var y = rect.height - self.axisHeight - height * 16 / 40
            var path = UIBezierPath(rect: CGRect(x: vspace, y: y, width: rect.width - 2 * vspace, height: 0.5))
            UIColor(hex: "D4D4D4").setFill()
            path.fill()
            
            var text = NSString(format: "16")
            var size = text.boundingRect(with: CGSize(), options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            text.draw(at: CGPoint(x: vspace - size.width - 1, y: y - size.height / 2), withAttributes: attributes)
            
            
            y = rect.height - self.axisHeight - height * 26 / 40
            path = UIBezierPath(rect: CGRect(x: vspace, y: y, width: rect.width - 2 * vspace, height: 0.5))
            UIColor(hex: "D4D4D4").setFill()
            path.fill()
            
            text = NSString(format: "26")
            size = text.boundingRect(with: CGSize(), options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            text.draw(at: CGPoint(x: vspace - size.width - 1, y: y - size.height / 2), withAttributes: attributes)
            
        } else {
            // 30%为干燥区域
            // 30%-70%为舒适区
            // 70%以上为潮湿区域
            // 湿度区间0-100%
            let height = rect.height - self.axisHeight - self.titleHeight
            var y = rect.height - self.axisHeight - height * 30 / 100
            var path = UIBezierPath(rect: CGRect(x: vspace, y: y, width: rect.width - 2 * vspace, height: 0.5))
            UIColor(hex: "D4D4D4").setFill()
            path.fill()
            
            var text = NSString(format: "30")
            var size = text.boundingRect(with: CGSize(), options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            text.draw(at: CGPoint(x: vspace - size.width - 1, y: y - size.height / 2), withAttributes: attributes)
            
            y = rect.height - self.axisHeight - height * 70 / 100
            path = UIBezierPath(rect: CGRect(x: vspace, y: y, width: rect.width - 2 * vspace, height: 0.5))
            UIColor(hex: "D4D4D4").setFill()
            path.fill()
            
            text = NSString(format: "70")
            size = text.boundingRect(with: CGSize(), options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            text.draw(at: CGPoint(x: vspace - size.width - 1, y: y - size.height / 2), withAttributes: attributes)
            
        }
        attributes[.kern] = 0.17
    }
    
    override func draw(_ rect: CGRect) {
        // 背景
        let path = UIBezierPath(roundedRect: rect, cornerRadius: 8)
        UIColor.white.setFill()
        path.fill()
        
        // 标题
        self.drawTitle(rect: rect)
        self.drawAxis(rect: rect)
        
        // 绘制曲线
        guard let entries = self.data?.entries else {
            return
        }
        if entries.isEmpty {
            return
        }
        
        let toRect: (CGPoint) -> CGPoint = { point in
            let x = self.vspace + (rect.width - 2 * self.vspace) * point.x
            let y = self.titleHeight + (rect.height - self.titleHeight - self.axisHeight) * (1 - point.y)
            return CGPoint(x: x, y: y)
        }
        
        let pathStroke = UIBezierPath()
        pathStroke.move(to: toRect(entries.first!))
        for v in entries {
            pathStroke.addLine(to: toRect(v))
        }
        UIColor(hex: "0098FF").setStroke()
        pathStroke.lineWidth = 1.5
        pathStroke.lineCapStyle = .round
        pathStroke.lineJoinStyle = .round
        pathStroke.stroke()
        
        pathStroke.addLine(to: CGPoint(x: rect.width - vspace, y: rect.height - self.axisHeight))
        pathStroke.addLine(to: CGPoint(x: vspace, y: rect.height - self.axisHeight))
        pathStroke.close()
        
        let gradient = CGGradient(
              colorsSpace: nil,
              colors: [
                  UIColor(hex: "0098FF").cgColor,
                  UIColor(hex: "ffffff", alpha: 0.000).cgColor] as CFArray,
              locations: [0, 1])!
        pathStroke.usesEvenOddFillRule = true
        let context = UIGraphicsGetCurrentContext()!
        context.saveGState()
        pathStroke.addClip()
        context.drawLinearGradient(gradient,
                                   start: CGPoint(x: 0, y: self.titleHeight),
                                   end: CGPoint(x: 0, y: rect.height - self.axisHeight - 30),
                                   options: .drawsAfterEndLocation)
        context.restoreGState()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: -配件属性记录统计分析
fileprivate class CharacteristicStatistics {
    struct Records: Codable {
        var aid: String
        var sid: Int
        var cid: Int
        var before: String
        var after: String
        struct Record: Codable {
            var time: String
            var value: AnyJSONType
        }
        var logs: [Record]
    }
    
    var entries = [CGPoint]()
    var average: Float = 0
    
    init? (data: Data, type: ChartType) {
        guard let msg = try? JSONDecoder().decode(Records.self, from: data) else {
            return nil
        }
        
        if msg.logs.isEmpty {
            return nil
        }
        
        var logs = msg.logs
        // 其实时间是否有值
        if logs.first!.time != msg.after {
            logs.insert(Records.Record(time: msg.after, value: logs.first!.value), at: 0)
        }
        // 结束时间是否有值
        if logs.last!.time != msg.before {
            logs.append(Records.Record(time: msg.before, value: logs.last!.value))
        }
    
        // 计算平均值
        for i in 1 ..< logs.count {
            let interval = logs[i].time.toDate().timeIntervalSince(logs[i-1].time.toDate())
            average += Float(interval) * (logs[i].value.toValueFloat()! + logs[i-1].value.toValueFloat()!) / 2
        }
        let interval = logs.last!.time.toDate().timeIntervalSince(logs.first!.time.toDate())
        average = average / Float(interval)
        
        var height: Float = 40.0
        if type == .humidity {
            height = 100.0
        }
        
        let tStart = msg.after.toDate()
        for i in 0 ..< logs.count {
            let xp = logs[i].time.toDate().timeIntervalSince(tStart) / interval
            let yp = (logs[i].value.toValueFloat()!) / height
            self.entries.append(CGPoint(x: xp, y: Double(yp)))
        }
    }
}




// MARK: - 温度数据
fileprivate let TemperatureData = """
{"after":"2019-10-05 14:30:28","aid":"00158d0002379233","before":"2019-10-06 14:30:28","cid":1,"limit":0,"logs":[{"time":"2019-10-05 14:59:38","value":27.2},{"time":"2019-10-05 15:44:44","value":27.71},{"time":"2019-10-05 16:40:59","value":28.09},{"time":"2019-10-05 16:41:04","value":28.24},{"time":"2019-10-05 16:41:10","value":28.33},{"time":"2019-10-05 16:41:30","value":28.43},{"time":"2019-10-05 16:55:34","value":27.89},{"time":"2019-10-05 20:14:08","value":27.37},{"time":"2019-10-05 20:19:09","value":26.85},{"time":"2019-10-05 20:26:22","value":26.32},{"time":"2019-10-05 20:34:34","value":25.75},{"time":"2019-10-05 20:47:53","value":25.25},{"time":"2019-10-05 21:04:29","value":24.71},{"time":"2019-10-05 22:03:37","value":24.2},{"time":"2019-10-06 03:31:58","value":23.88},{"time":"2019-10-06 07:19:32","value":24.11},{"time":"2019-10-06 08:00:40","value":24.61},{"time":"2019-10-06 08:11:34","value":25.12},{"time":"2019-10-06 08:18:16","value":25.44},{"time":"2019-10-06 08:31:05","value":25.99},{"time":"2019-10-06 09:23:21","value":26.5},{"time":"2019-10-06 09:30:53","value":27.01},{"time":"2019-10-06 09:47:39","value":27.51},{"time":"2019-10-06 10:23:14","value":28.06},{"time":"2019-10-06 11:20:56","value":27.56},{"time":"2019-10-06 11:41:38","value":27.01},{"time":"2019-10-06 11:56:38","value":26.51},{"time":"2019-10-06 12:03:40","value":26},{"time":"2019-10-06 12:12:53","value":25.5},{"time":"2019-10-06 12:24:26","value":25},{"time":"2019-10-06 12:45:08","value":24.78},{"time":"2019-10-06 12:54:06","value":25.3},{"time":"2019-10-06 13:04:39","value":25.81},{"time":"2019-10-06 13:17:38","value":26.32},{"time":"2019-10-06 13:49:13","value":26.84},{"time":"2019-10-06 14:13:51","value":27.34}],"sid":1}
""".data(using: .utf8)!


fileprivate let HumidityData = """
{"after":"2019-10-05 14:30:28","aid":"00158d0002379233","before":"2019-10-06 14:30:28","cid":2,"limit":0,"logs":[{"time":"2019-10-05 14:59:38","value":66},{"time":"2019-10-05 15:44:44","value":65},{"time":"2019-10-05 16:40:59","value":77},{"time":"2019-10-05 16:41:04","value":84},{"time":"2019-10-05 16:41:10","value":77},{"time":"2019-10-05 16:41:15","value":71},{"time":"2019-10-05 16:41:30","value":65},{"time":"2019-10-05 16:55:34","value":63},{"time":"2019-10-05 20:14:08","value":58},{"time":"2019-10-05 20:19:09","value":53},{"time":"2019-10-05 20:26:22","value":50},{"time":"2019-10-05 20:34:34","value":48},{"time":"2019-10-05 20:47:53","value":47},{"time":"2019-10-05 21:04:29","value":48},{"time":"2019-10-05 22:03:37","value":49},{"time":"2019-10-06 03:31:58","value":55},{"time":"2019-10-06 07:19:32","value":61},{"time":"2019-10-06 08:00:40","value":60},{"time":"2019-10-06 08:11:34","value":62},{"time":"2019-10-06 08:18:16","value":68},{"time":"2019-10-06 08:31:05","value":72},{"time":"2019-10-06 09:23:21","value":71},{"time":"2019-10-06 09:30:53","value":70},{"time":"2019-10-06 09:47:39","value":68},{"time":"2019-10-06 10:23:14","value":66},{"time":"2019-10-06 11:20:56","value":63},{"time":"2019-10-06 11:41:38","value":64},{"time":"2019-10-06 11:56:38","value":58},{"time":"2019-10-06 12:03:40","value":53},{"time":"2019-10-06 12:12:53","value":51},{"time":"2019-10-06 12:24:26","value":49},{"time":"2019-10-06 12:45:08","value":55},{"time":"2019-10-06 12:54:06","value":56},{"time":"2019-10-06 13:04:40","value":59},{"time":"2019-10-06 13:17:38","value":60},{"time":"2019-10-06 13:49:13","value":64}],"sid":1}
""".data(using: .utf8)!


