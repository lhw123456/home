//
//  ViewController.swift
//  ui-scratch
//
//  Created by Yun Zeng on 2019/8/27.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.lightGray
    }
    
    
    override func viewDidAppear(_ animated: Bool)  {
        super.viewDidAppear(animated)
        let controller = UILampViewController()
        controller.modalPresentationStyle = .formSheet
        controller.isModalInPresentation = false
        controller.preferredContentSize = CGSize(width: 375, height: 680)
        self.present(controller, animated: true)
    }
    
}
