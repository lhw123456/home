//
//  AutomationBarCustom.swift
//  home
//
//  Created by Yun Zeng on 2019/8/5.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class AutomationCustomBarView: UIView {
    
    public var opacity: Float = 0 {
        didSet {
            if opacity > 1.0 {
                opacity = 1.0
            }
            
            self.blurView.layer.opacity = opacity
            if opacity > 0.95 {
                self.title = "自动化"
            } else if opacity < 0.9 {
                self.title = ""
            }
        }
    }
    
    private lazy var blurView: UIVisualEffectView = {
        let blur =  UIVisualEffectView()
        blur.effect = UIBlurEffect(style: .light)
        blur.translatesAutoresizingMaskIntoConstraints = false
        blur.clipsToBounds = true
        blur.layer.opacity = 0
        return blur
    }()
    
    private lazy var addImage: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = UIColor.lightGray
        view.layer.cornerRadius = 13
        view.image = UIImage(named: "add")
        return view
    }()
    
    public var title: String = "" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold),
                NSAttributedString.Key.kern: -0.41,
                NSAttributedString.Key.foregroundColor: UIColor.black]
            self.titleLabel.attributedText = NSAttributedString(string: title, attributes: attributes)
        }
    }
    
    fileprivate lazy var titleLabel: UILabel = {
        let label = UILabel()
        let attributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold),
            NSAttributedString.Key.kern: -0.41,
            NSAttributedString.Key.foregroundColor: UIColor.black]
        label.attributedText = NSAttributedString(string: self.title, attributes: attributes)
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    
    func initialize() {
        self.backgroundColor = UIColor.clear
        
        self.addSubview(self.blurView)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        blurView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        blurView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        blurView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        blurView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        
        self.addSubview(self.addImage)
        addImage.translatesAutoresizingMaskIntoConstraints = false
        addImage.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -18).isActive = true
        addImage.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).isActive = true
        addImage.heightAnchor.constraint(equalToConstant: 26).isActive = true
        addImage.widthAnchor.constraint(equalToConstant: 26).isActive = true
        
        self.addSubview(self.titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        titleLabel.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
}


extension AutomationController: UIScrollViewDelegate {
    func initNavigationBar() {
        self.contentsView.delegate = self
        let navigationBar = self.navigationController!.navigationBar
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.clear]
        navigationBar.shadowImage = UIImage()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        var offset: CGFloat = (scrollView.contentOffset.y + 74) / 40
        if offset > 2 {
            offset = 2
        } else if offset < 0 {
            offset = 0
        }
        self.customBarView.opacity = Float(offset)
    }
}

class AutoLargeTitleView: UIView {
    
    private let xOffset: CGFloat = 18
    var title: String = "" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 32, weight: UIFont.Weight.medium),
                NSAttributedString.Key.kern: 0.39,
                NSAttributedString.Key.foregroundColor: UIColor.black]
            self.autoLabel.attributedText = NSAttributedString(string: title, attributes: attributes)
        }
    }
    
    var desc: String = "" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium),
                NSAttributedString.Key.kern: 0.29,
                NSAttributedString.Key.foregroundColor: UIColor(red: 50/255, green: 50/255, blue: 50/255, alpha: 1)]
            self.descLabel.attributedText = NSAttributedString(string: desc, attributes: attributes)
        }
    }
    
    private lazy var autoLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    func initialize() {
        self.addSubview(autoLabel)

        autoLabel.translatesAutoresizingMaskIntoConstraints = false
        autoLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        autoLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: xOffset).isActive = true
        autoLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        autoLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.addSubview(descLabel)
        descLabel.translatesAutoresizingMaskIntoConstraints = false
        descLabel.topAnchor.constraint(equalTo: autoLabel.bottomAnchor, constant: 0).isActive = true
        descLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: xOffset + 2).isActive = true
        descLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        descLabel.heightAnchor.constraint(equalToConstant: 24).isActive = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
}



