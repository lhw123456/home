//
//  ViewController.swift
//  home
//
//  Created by Yun Zeng on 2019/8/4.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    lazy var bgImg: UIImageView = {
        return UIImageView()
    } ()
    
    lazy var scrollView: UIScrollView = {
        return UIScrollView()
    } ()
    
    private var customBar: CustomBarView!
    private var summaryView: SummaryView!
    private var scenes: ScenesCollectionView!
    private var services: ServiceCollectionView!
    
    var label1: UILabel?
    
    override func viewLayoutMarginsDidChange() {
        super.viewLayoutMarginsDidChange()
    }
    
    var home: Home?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        home = HomeManager.instance.current
        guard let home = HomeManager.instance.current else {
            return
        }

        // 背景图片
        bgImg.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.bgImg)
        bgImg.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        bgImg.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        bgImg.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        bgImg.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        bgImg.image = UIImage(named: "pic-bg")
        bgImg.contentMode = .scaleAspectFill
        
        // 导航栏透明
        let navigationBar = self.navigationController!.navigationBar
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.clear]
        navigationBar.shadowImage = UIImage()
        
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(containerView)

        containerView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        self.customBar = CustomBarView()
        self.view.addSubview(self.customBar)
        customBar.opacity = 1
        customBar.title = home.name
        self.customBar.translatesAutoresizingMaskIntoConstraints = false
        self.customBar.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.customBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.customBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.customBar.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true

        self.scrollView.delaysContentTouches = false

        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        containerView.addSubview(self.scrollView)
        self.scrollView.delegate = self

        scrollView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        
        self.summaryView = SummaryView(manager: home.serviceManager)
        summaryView.home = home.name
        self.scrollView.addSubview(summaryView)
        summaryView.translatesAutoresizingMaskIntoConstraints = false
        summaryView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        summaryView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        summaryView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        summaryView.heightAnchor.constraint(equalToConstant: 130).isActive = true
        
        self.scenes = ScenesCollectionView(manager: home.sceneManager)
        self.scrollView.addSubview(self.scenes)
        scenes.controller = self
        scenes.translatesAutoresizingMaskIntoConstraints = false
        scenes.topAnchor.constraint(equalTo: summaryView.bottomAnchor).isActive = true
        scenes.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        scenes.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        self.contraintSceneView = scenes.heightAnchor.constraint(equalToConstant: 180)
        contraintSceneView.isActive = true
        
        self.services = ServiceCollectionView(frame: .zero, manager: home.serviceManager)
        self.scrollView.addSubview(self.services)
        services.controller = self
        services.translatesAutoresizingMaskIntoConstraints = false
        services.topAnchor.constraint(equalTo: scenes.bottomAnchor).isActive = true
        services.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        services.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        contraintServiceView = services.heightAnchor.constraint(equalToConstant: 630)
        contraintServiceView.isActive = true
         // 930
        
        scrollView.contentSize = CGSize(width: self.view.bounds.width, height: self.view.frame.height + 1)
//        scrollView.contentSize = CGSize(width: self.view.bounds.width, height: self.view.frame.height + 1)
    }
    
    var contraintServiceView: NSLayoutConstraint!
    var contraintSceneView: NSLayoutConstraint!
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {

        super.viewWillTransition(to: size, with: coordinator)
        Logger.Info("viewWillTransition: \(self.view.bounds.size)")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.updateLayout()
        Logger.Info("viewDidLayoutSubviews: \(self.view.bounds.size)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Logger.Info("viewDidAppear: \(self.view.bounds.size)")
    }
    
    func updateLayout() {
        if home == nil {
            return
        }
        guard let serviceViewHeight = self.services.contentHeight else {
            return
        }
        contraintServiceView.isActive = false
        contraintServiceView = services.heightAnchor.constraint(equalToConstant: CGFloat(serviceViewHeight))
        contraintServiceView.isActive = true
        guard let sceneViewHeight = self.scenes.contentHeight else {
            return
        }
        contraintSceneView.isActive = false
        contraintSceneView = scenes.heightAnchor.constraint(equalToConstant: CGFloat(sceneViewHeight))
        contraintSceneView.isActive = true
        
        // ScrollView高度计算
        let scrollHeight = CGFloat(130 + serviceViewHeight + sceneViewHeight)
        if scrollHeight >= self.view.frame.height + 1 {
            scrollView.alwaysBounceVertical = true
            scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: scrollHeight + 20.0)
        } else {
            scrollView.alwaysBounceVertical = false
            let height = self.view.frame.height + 1
            scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: height)
        }

        UIView.animate(withDuration: 0.3) {
            self.scenes.layoutIfNeeded()
            self.services.layoutIfNeeded()
            self.scrollView.layoutIfNeeded()
        }
    }
    
}

extension HomeViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.scenes.setStatic()
        self.services.setStatic()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var offset: CGFloat = CGFloat(scrollView.contentOffset.y + self.customBar.frame.height) / 50
        if offset > 1 {
            offset = 1
        } else if offset < 0 {
            offset = 0
        }
        self.customBar.opacity = Float(offset)
    }
}




