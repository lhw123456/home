//
//  SceneCellView.swift
//  home
//
//  Created by Yun Zeng on 2019/1/28.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit
import AudioToolbox

class SceneCell: UICollectionViewCell {
    static let identifier = "sceneCellViewIdentifier"
    
    var sceneView: SceneCellView!    
    var action: ((_ scene: SceneCell) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.sceneView = SceneCellView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        self.addSubview(self.sceneView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setStatic() {
        if self.bTouched {
            UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 10.0, options: [], animations: {
                self.transform = .identity
            })
        }
        if self.isPlayed {
            self.isPlayed = false
        }
    }
    
    private var bTouched = false
    
    fileprivate var feedbackThreshold: Double = 0.1
    var isPlayed: Bool = false {
        didSet {
            if self.isPlayed == false {
                UIView.animate(withDuration: 0.2, delay: 0.5, usingSpringWithDamping: 0.3, initialSpringVelocity: 10.0, options: [], animations: {
                    self.transform = .identity
                })
            }
        }
    }
    
    // 压力感应
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        bTouched = true
        Logger.Info("touchesBegan")
        UIView.animate(withDuration: 0.1) {
           self.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        if traitCollection.forceTouchCapability == .available {
            if let touch = touches.first {
                let rate = touch.force / touch.maximumPossibleForce
                let scaling = 1.0 + (rate / 2) + 0.3
                UIView.animate(withDuration: 0.1, animations: {
                    self.transform = CGAffineTransform(scaleX: scaling, y: scaling)
                })
                if Double(rate) >= feedbackThreshold {
                    if !self.isPlayed {
                        AudioServicesPlaySystemSound(1520)
                        self.isPlayed = true
                        self.action?(self)
                    }
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        // 已经进入详情页面,停止还原动画
        // 等从场景详情页面返回时，再还原图标大小
        if self.isPlayed == false {
            UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 10.0, options: [], animations: {
                self.transform = .identity
            })
        }
        self.bTouched = false
    }
    
    // 设置部分
    func setScene(scene: HomeScene, action: ((_ scene: SceneCell) -> Void)? = nil) -> Bool {
        self.action = action
        var result = false
        if scene.name != self.sceneView.title {
            self.sceneView.title = scene.name
            result = true
        }
        if scene.subtitle != self.sceneView.subtitle {
            self.sceneView.subtitle = scene.subtitle
            result = true
        }
        if scene.stateIcon != self.sceneView.icon {
            self.sceneView.icon = scene.stateIcon
            result = true
        }
        if scene.state != self.sceneView.state {
            self.sceneView.state = scene.state
            result = true
        }
        return result
    }
}




