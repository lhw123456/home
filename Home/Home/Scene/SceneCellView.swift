//
//  SceneCellView.swift
//  home
//
//  Created by Yun Zeng on 2019/1/28.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit


class SceneCellView: UIView {
    // 图标
    var icon = "" {
        didSet {
            if self.icon != "" {
                self.iconImage.image = UIImage(named: self.icon)
            }
        }
    }
    
    fileprivate lazy var iconImage: UIImageView = {
        let view = UIImageView(frame: CGRect(x: 10, y: 10, width: 40, height: 40))
        view.image = UIImage(named: "morning-on")
        return view
    }()
    
    var state: SceneState = SceneState.idle {
        didSet {
            if self.state != .idle {
                self.removeBlurView()
            } else {
                self.addBlurView(style: .light)
            }
        }
    }
    
    // 模糊背景显示
    private lazy var blurView: UIVisualEffectView = {
        return UIVisualEffectView(effect: nil)
    }()
    
    private func addBlurView(style: UIBlurEffect.Style) {
        self.backgroundColor = .clear
        self.blurView.effect = UIBlurEffect(style: .light)
        self.blurView.translatesAutoresizingMaskIntoConstraints = false
        self.insertSubview(self.blurView, at: 0)
        self.blurView.frame = self.frame
        self.blurView.clipsToBounds = true
        self.blurView.backgroundColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 0.7)
        self.blurView.layer.cornerRadius = 13
        self.blurView.layer.opacity = 1
    }
    
    private func removeBlurView() {
        self.backgroundColor = UIColor.white
        self.blurView.layer.opacity = 0
    }
    
    // 标题
    var title = "" {
        didSet {
            let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium),
                NSAttributedString.Key.foregroundColor: UIColor(red: 98/255, green: 98/255, blue: 98/255, alpha: 1),
                NSAttributedString.Key.kern: 0.46]
            self.titleLabel.attributedText = NSAttributedString(string: title, attributes: attributes)
        }
    }
    
    fileprivate lazy var titleLabel: UILabel = {
        let y = self.subtitle == "" ? 19 : 12
        let label = UILabel(frame: CGRect(x: 55, y: y, width: 100, height: 22))
        return label
    }()
    
    // 副标题
    var subtitle = "" {
        didSet {
            if subtitle != "" {
                let attributes: [NSAttributedString.Key : Any] = [
                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: 11, weight: UIFont.Weight.semibold),
                    NSAttributedString.Key.kern: -0.18,
                    NSAttributedString.Key.foregroundColor: UIColor(red: 114/255, green: 114/255, blue: 114/255, alpha: 1)]
                self.subtitleLabel.attributedText = NSAttributedString(string: subtitle, attributes: attributes)
                if self.subtitleLabel.superview == nil {
                    self.addSubview(self.subtitleLabel)
                }
                self.titleLabel.frame = CGRect(x: 55, y: 12, width: 100, height: 22)
            } else {
                self.subtitleLabel.removeFromSuperview()
                self.titleLabel.frame = CGRect(x: 55, y: 19, width: 100, height: 22)
            }
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    
    fileprivate lazy var subtitleLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 55, y: 34, width: 100, height: 13))
        return label
    }()
    
    func initialize() {
        self.addSubview(titleLabel)
        self.addSubview(iconImage)
        if self.subtitle != "" {
            self.addSubview(subtitleLabel)
        }
        if self.state == .activated {
            self.removeBlurView()
        } else {
            self.addBlurView(style: .light)
        }
        self.layer.cornerRadius = 13
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
