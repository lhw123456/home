//
//  ServicePanelMotion.swift
//  home
//
//  Created by Yun Zeng on 2019/9/27.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class ServicePanelSensorMotion: ServicePanelBase {
    
    var motion: ServiceSensorMotion
    
    init(motion: ServiceSensorMotion) {
        self.motion = motion
        super.init(service: motion.service)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customBar.subtitle = self.motion.desc
        self.view.backgroundColor = UIColor(hex: "d7d7d7")
        
        let modeOpen = MotionModeView(frame: .zero, type: .open)
        self.view.addSubview(modeOpen)
        modeOpen.translatesAutoresizingMaskIntoConstraints = false
        modeOpen.topAnchor.constraint(equalTo: self.customBar.bottomAnchor, constant: 50).isActive = true
        modeOpen.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        modeOpen.heightAnchor.constraint(equalToConstant: 120).isActive = true
        modeOpen.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15).isActive = true
        modeOpen.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -15).isActive = true
        
        modeOpen.trigger.mode = "人体感应灯"
        modeOpen.trigger.icon = "panel-icon-sensor-motion-open"
        modeOpen.trigger.condition = "有人在家\n环境光照小于70lux"
        
        let motionOpenEdit = UIButton()
        motionOpenEdit.setTitle("人体感应灯功能设置", for: .normal)
        motionOpenEdit.contentHorizontalAlignment = .center
        motionOpenEdit.setTitleColor(UIColor(hex: "007AFF"), for: .normal)
        motionOpenEdit.setTitleColor(UIColor(hex: "007AFF", alpha: 0.6), for: .highlighted)
        
        self.view.addSubview(motionOpenEdit)
        motionOpenEdit.translatesAutoresizingMaskIntoConstraints = false
        motionOpenEdit.topAnchor.constraint(equalTo: modeOpen.bottomAnchor, constant: 40).isActive = true
        motionOpenEdit.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
        motionOpenEdit.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30).isActive = true
        motionOpenEdit.heightAnchor.constraint(equalToConstant: 44).isActive = true
        motionOpenEdit.backgroundColor = UIColor.white
        motionOpenEdit.layer.cornerRadius = 10
        
        
        let modeAlarm = MotionModeView(frame: .zero, type: .alarm)
        self.view.addSubview(modeAlarm)
        modeAlarm.translatesAutoresizingMaskIntoConstraints = false
        modeAlarm.topAnchor.constraint(equalTo: motionOpenEdit.bottomAnchor, constant: 80).isActive = true
        modeAlarm.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        modeAlarm.heightAnchor.constraint(equalToConstant: 120).isActive = true
        modeAlarm.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15).isActive = true
        modeAlarm.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -15).isActive = true
        
        modeAlarm.trigger.mode = "安防报警"
        modeAlarm.trigger.icon = "panel-icon-sensor-motion-alarm"
        modeAlarm.trigger.condition = "无人在家"
        
        let motionAlarmEdit = UIButton()
        motionAlarmEdit.setTitle("安防报警功能设置", for: .normal)
        motionAlarmEdit.contentHorizontalAlignment = .center
        motionAlarmEdit.setTitleColor(UIColor(hex: "007AFF"), for: .normal)
        motionAlarmEdit.setTitleColor(UIColor(hex: "007AFF", alpha: 0.6), for: .highlighted)
        
        self.view.addSubview(motionAlarmEdit)
        motionAlarmEdit.translatesAutoresizingMaskIntoConstraints = false
        motionAlarmEdit.topAnchor.constraint(equalTo: modeAlarm.bottomAnchor, constant: 25).isActive = true
        motionAlarmEdit.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
        motionAlarmEdit.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30).isActive = true
        motionAlarmEdit.heightAnchor.constraint(equalToConstant: 44).isActive = true
        motionAlarmEdit.backgroundColor = UIColor.white
        motionAlarmEdit.layer.cornerRadius = 10
        
    }
}

// 人体红外检测传感器
// 条件出发UIView
fileprivate class MotionTriggerView: UIView {
    var icon: String = "" {
        didSet {
            self.iconImage.image = UIImage(named: icon)
        }
    }
    
    var mode: String = "" {
        didSet {
            self.title.attributedText = NSAttributedString(string: self.mode, attributes: self.titleAttributes)
        }
    }
    
    var condition: String = "" {
        didSet {
            self.subtitle.attributedText = NSAttributedString(string: self.condition, attributes: self.subtitleAttributes)
        }
    }
    
    fileprivate let titleAttributes: [NSAttributedString.Key : Any] =
        [
            .font: UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular),
            .foregroundColor: UIColor(hex: "000000"),
            .kern: 0.32
        ]
    
    fileprivate let subtitleAttributes: [NSAttributedString.Key : Any] = {
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 2
        style.alignment = .center
        let attributes: [NSAttributedString.Key : Any] =
            [
                   .font: UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular),
                   .foregroundColor: UIColor(hex: "858585"),
                   .kern: 0.39,
                   .paragraphStyle: style
             ]
        return attributes
    } ()
       
    private lazy var iconImage: UIImageView = {
        let view = UIImageView()
        return view
    } ()
    
    private lazy var title: UILabel = {
        let label = UILabel()
        return label
    } ()
    
    
    private lazy var subtitle: UILabel = {
        let label = UILabel()
        return label
    } ()
    
    func initialize() {
        
        self.addSubview(self.title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        title.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        title.heightAnchor.constraint(equalToConstant: 21).isActive = true
        title.textAlignment = .center
        
        self.addSubview(self.iconImage)
        iconImage.translatesAutoresizingMaskIntoConstraints = false
        iconImage.heightAnchor.constraint(equalToConstant: 44).isActive = true
        iconImage.widthAnchor.constraint(equalToConstant: 44).isActive = true
        iconImage.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        iconImage.bottomAnchor.constraint(equalTo: self.title.topAnchor, constant: -2).isActive = true
        
        self.addSubview(self.subtitle)
        subtitle.translatesAutoresizingMaskIntoConstraints = false
        subtitle.topAnchor.constraint(equalTo: self.title.bottomAnchor, constant: 2).isActive = true
        subtitle.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        subtitle.numberOfLines = 2
        
        subtitle.autoresizingMask = .flexibleWidth
        subtitle.textAlignment = .center
        subtitle.lineBreakMode = .byWordWrapping
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
}

fileprivate class MotionModeView: UIView {
    
    enum TriggerType: Int {
        case open
        case alarm
    }
    var type: TriggerType = .open
    
    lazy var trigger: MotionTriggerView = {
        let view = MotionTriggerView()
        return view
    } ()
    
    func initialize() {
        
        let arrow = UIImageView()
        arrow.image = UIImage(named: "panel-icon-sensor-motion-arrow")
        self.addSubview(arrow)
        arrow.translatesAutoresizingMaskIntoConstraints = false
        arrow.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: -10).isActive = true
        arrow.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        arrow.heightAnchor.constraint(equalToConstant: 30).isActive = true
        arrow.widthAnchor.constraint(equalToConstant: 70).isActive = true
        
        self.addSubview(self.trigger)
        trigger.translatesAutoresizingMaskIntoConstraints = false
        trigger.centerYAnchor.constraint(equalTo: arrow.centerYAnchor).isActive = true
        trigger.trailingAnchor.constraint(equalTo: arrow.leadingAnchor).isActive = true
        trigger.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        
        if self.type == .alarm {
            let label = UILabel()
            self.addSubview(label)
            label.translatesAutoresizingMaskIntoConstraints = false
            label.centerYAnchor.constraint(equalTo: arrow.centerYAnchor).isActive = true
            label.leadingAnchor.constraint(equalTo: arrow.trailingAnchor, constant: 20).isActive = true
            label.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
            label.attributedText = NSAttributedString(string: "安防报警推送", attributes: self.trigger.titleAttributes)
            label.textAlignment = .left
            label.autoresizingMask = .flexibleWidth
        } else {
            let service = ServiceCellView(frame: .zero)
            self.addSubview(service)
            service.translatesAutoresizingMaskIntoConstraints = false
            service.widthAnchor.constraint(equalToConstant: 105).isActive = true
            service.heightAnchor.constraint(equalToConstant: 105).isActive = true
            service.centerYAnchor.constraint(equalTo: arrow.centerYAnchor).isActive = true
            service.leadingAnchor.constraint(equalTo: arrow.trailingAnchor, constant: 20).isActive = true
            service.state = .highlight
            service.name = "床头灯"
            service.desc = "30%"
            service.room = "卧室"
            let imageView = UIImageView(frame: ServiceCellView.iconSize)
            imageView.image = UIImage(named: "service-light-hue-go-on")
            service.iconView = imageView
        }
    }
    
    init(frame: CGRect, type: TriggerType) {
        self.type = type
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
}












