//
//  ServicePanelCoordinator.swift
//  home
//
//  Created by Yun Zeng on 2019/9/26.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class ServicePanelCoordinator: ServicePanelBase {
    var coordinator: ServiceCoordinator
    
    init(coordinator: ServiceCoordinator) {
        self.coordinator = coordinator
        super.init(service: coordinator.service)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var topologicalImageView: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "panel-coordinator-struct-diagram")
        view.autoresizingMask = .flexibleHeight
        return view
    } ()
    
    private lazy var permitJionLabel: CharacteristicOnSetLabel = {
        let setting = CharacteristicOnSetLabel(title: "子设备入网许可", value: self.coordinator.permitJoinValue)
        return setting
    } ()
    
    private lazy var subdeviceLabel: CharacteristicInfoLabel = {
        let label = CharacteristicInfoLabel(title: "子设备数量", value: "12", detail: true)
        return label
    } ()
    
    private lazy var channelLabel: CharacteristicInfoLabel = {
        let channel = self.coordinator.channel.value?.toValueInt() ?? 0
        let label = CharacteristicInfoLabel(title: "信号", value: String(format: "%d", channel))
        return label
    } ()
    
    private lazy var netAddrLabel: CharacteristicInfoLabel = {
        let netAddr = self.coordinator.netAddr.value?.toString() ?? ""
        let label = CharacteristicInfoLabel(title: "网络地址", value: netAddr)
        return label
    } ()
    
    private lazy var macAddrLabel: CharacteristicInfoLabel = {
        let macAddr = self.coordinator.macAddr.value?.toString() ?? ""
        let label = CharacteristicInfoLabel(title: "物理地址", value: macAddr)
        return label
    } ()

    override func updateService() {
        self.permitJionLabel.value = self.coordinator.permitJoinValue
        let channel = self.coordinator.channel.value?.toValueInt() ?? 0
        self.channelLabel.value = String(format: "%d", channel)
        let netAddr = self.coordinator.netAddr.value?.toString() ?? ""
        self.netAddrLabel.value = netAddr
        let macAddr = self.coordinator.macAddr.value?.toString() ?? ""
        self.macAddrLabel.value = macAddr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(hex: "D7D7D7")
        self.customBar.subtitle = "12个子设备在线"
        
        self.view.addSubview(self.topologicalImageView)
        topologicalImageView.translatesAutoresizingMaskIntoConstraints = false
        topologicalImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        topologicalImageView.topAnchor.constraint(equalTo: self.customBar.bottomAnchor, constant: 20).isActive = true
    
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: self.topologicalImageView.bottomAnchor, constant: 10).isActive = true
        stackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 18).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -18).isActive = true
        stackView.customize(backgroundColor: UIColor(hex: "EBEBEB"), radiusSize: 12)
        
        stackView.addCustomView(view: self.permitJionLabel, pos: .top)
        stackView.addCustomView(view: self.subdeviceLabel)
        stackView.addCustomView(view: self.channelLabel)
        stackView.addCustomView(view: self.netAddrLabel)
        stackView.addCustomView(view: self.macAddrLabel, pos: .bottom)
        
        permitJionLabel.onSet = { set in
            if set {
                self.coordinator.setPermitJion(interval: 100)
            } else {
                self.coordinator.setPermitJion(interval: 0)
            }
        }
    }
}

// MARK: - 服务属性详情标签
class CharacteristicInfoLabel: UIView {
    
    private let titleAttrs: [NSAttributedString.Key : Any] = [
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular),
        NSAttributedString.Key.kern: -0.41,
        NSAttributedString.Key.foregroundColor: UIColor.black]
    
    private let valueAttrs: [NSAttributedString.Key : Any] = [
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular),
        NSAttributedString.Key.kern: 0.19,
        NSAttributedString.Key.foregroundColor: UIColor(hex: "24242446")]
    
    var title: String {
        didSet {
            self.titleLabel.attributedText = NSAttributedString(string: self.title, attributes: self.titleAttrs)
        }
    }
    
    var value: String {
        didSet {
            self.valueLabel.attributedText = NSAttributedString(string: self.value, attributes: self.valueAttrs)
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.autoresizingMask = .flexibleWidth
        label.attributedText = NSAttributedString(string: self.title, attributes: self.titleAttrs)
        return label
    } ()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.autoresizingMask = .flexibleWidth
        label.attributedText = NSAttributedString(string: self.value, attributes: self.valueAttrs)
        return label
    } ()
    
    private lazy var detailIcon: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "label-icon-detail")
        return view
    } ()
    
    init(title: String, value: String, detail: Bool = false) {
        self.title = title
        self.value = value
        super.init(frame: .zero)
        
        self.addSubview(self.titleLabel)
        self.addSubview(self.valueLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15).isActive = true
        titleLabel.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        if detail {
            self.addSubview(self.detailIcon)
            detailIcon.translatesAutoresizingMaskIntoConstraints = false
            detailIcon.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
            detailIcon.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15).isActive = true
            detailIcon.widthAnchor.constraint(equalToConstant: 18).isActive = true
        }
        
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        valueLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        if detail {
            valueLabel.trailingAnchor.constraint(equalTo: self.detailIcon.leadingAnchor).isActive = true
        } else {
            valueLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15).isActive = true
        }
        valueLabel.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - 服务属性详情标签
class CharacteristicOnSetLabel: UIView {
    
    private let titleAttrs: [NSAttributedString.Key : Any] = [
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular),
        NSAttributedString.Key.kern: -0.41,
        NSAttributedString.Key.foregroundColor: UIColor.black]
    
    private let valueAttrs: [NSAttributedString.Key : Any] = [
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.regular),
        NSAttributedString.Key.kern: 0.19,
        NSAttributedString.Key.foregroundColor: UIColor(hex: "00C900")]
    
    private let onOff: UISwitch = {
        let ctrl = UISwitch(frame: .zero)
        return ctrl
    } ()
    
    var title: String {
        didSet {
            self.titleLabel.attributedText = NSAttributedString(string: self.title, attributes: self.titleAttrs)
        }
    }
    
    var value: Int {
        didSet {
            if value == 0 {
                self.valueLabel.isHidden = true
                self.onOff.isHidden = false
                self.onOff.setOn(false, animated: false)
            } else {
                self.valueLabel.isHidden = false
                self.onOff.isHidden = true
                self.valueLabel.attributedText = NSAttributedString(string: String(format: "%ds", value), attributes: self.valueAttrs)
            }
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.autoresizingMask = .flexibleWidth
        label.attributedText = NSAttributedString(string: self.title, attributes: self.titleAttrs)
        return label
    } ()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.autoresizingMask = .flexibleWidth
        return label
    } ()
    
    init(title: String, value: Int) {
        self.title = title
        self.value = value
        super.init(frame: .zero)
        
        self.addSubview(self.titleLabel)
        self.addSubview(self.valueLabel)
        self.addSubview(self.onOff)
        
        if value == 0 {
            self.valueLabel.isHidden = true
            self.onOff.isHidden = false
            self.onOff.setOn(false, animated: false)
        } else {
            self.valueLabel.isHidden = false
            self.onOff.isHidden = true
            self.valueLabel.attributedText = NSAttributedString(string: String(format: "%ds", value), attributes: self.valueAttrs)
        }
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15).isActive = true
        titleLabel.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        valueLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        valueLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15).isActive = true
        valueLabel.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        onOff.translatesAutoresizingMaskIntoConstraints = false
        onOff.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        onOff.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15).isActive = true
        onOff.addTarget(self, action: #selector(self.switchIsChanged(_:)), for: .valueChanged)
    }
    
    var onSet: ((Bool) -> Void)?
    @objc func switchIsChanged(_ sw: UISwitch) {
        self.onSet?(sw.isOn)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}




