//
//  ServicePanelButton.swift
//  home
//
//  Created by Yun Zeng on 2019/9/26.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

class ServicePanelSceneButton: ServicePanelBase {
    
    var button: ServiceSceneButton
    
    init(button: ServiceSceneButton) {
        self.button = button
        super.init(service: button.service)
        self.view.backgroundColor = UIColor(hex: "D8D8D8")

        guard let home = HomeManager.instance.current else {
            return
        }
        guard let automation = home.automationManager!.findAutomationByEventCharacteristic(characteristic: button.contactStateChar) else {
            return
        }
        
        let title = UILabel()
        let attributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.medium),
            NSAttributedString.Key.kern: 0.29,
            NSAttributedString.Key.foregroundColor: UIColor.black]
        title.textAlignment = .left
        title.autoresizingMask = .flexibleWidth
        title.attributedText = NSAttributedString(string: "开关控制", attributes: attributes)
        self.view.addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 25).isActive = true
        title.heightAnchor.constraint(equalToConstant: 20).isActive = true
        title.topAnchor.constraint(equalTo: self.customBar.bottomAnchor, constant: 24).isActive = true
        
        Logger.Info("automation: \(automation.description)")
        let manager = ActionsServiceManager(actions: automation.actions!, home: home)
        let subtitle = "按下按键将实现以下设备的开关翻转控制"
        let actionsView = ActionsCollectionView(frame: .zero, manager: manager, title: subtitle)
        view.addSubview(actionsView)
        
        actionsView.translatesAutoresizingMaskIntoConstraints = false
        actionsView.topAnchor.constraint(equalTo: title.bottomAnchor, constant: -3).isActive = true
        actionsView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        actionsView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        actionsView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        // 设置按键
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: self.customBar.bottomAnchor, constant: 460).isActive = true
        stackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 18).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -18).isActive = true
        stackView.customize(backgroundColor: UIColor.white, radiusSize: 10)
        
        let editBtn = UIButton()
        editBtn.setTitle("编辑关联设备", for: .normal)
        editBtn.setTitleColor(UIColor(red: 0, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
        editBtn.setTitleColor(UIColor(red: 0, green: 122/255, blue: 255/255, alpha: 0.6), for: .highlighted)
        stackView.addCustomControl(view: editBtn, height: 44, pos: .top)
        
        let changeBtn = UIButton()
        changeBtn.setTitle("切换至切换场景功能", for: .normal)
        changeBtn.setTitleColor(UIColor(red: 0, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
        changeBtn.setTitleColor(UIColor(red: 0, green: 122/255, blue: 255/255, alpha: 0.6), for: .highlighted)
        stackView.addCustomControl(view: changeBtn, height: 44, pos: .bottom)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}












