//
//  ServiceLightColorTemperature.swift
//  home
//
//  Created by Yun Zeng on 2019/10/5.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

import UIKit

// 宜家色温灯
class ServiceLightColorTemperature {
    var service: Service
    
    var on: HMCharacteristic
    var level: HMCharacteristic
    var temperature: HMCharacteristic
    
    init?(service: Service) {
        if service.type != HMServiceType.temperatureColorLight.rawValue {
            return nil
        }
        guard let on = service.findCharacteristic(type: .on),
            let level = service.findCharacteristic(type: .level),
            let temperature = service.findCharacteristic(type: .colorTemperature) else {
               return nil
        }
        
        self.service = service
        self.on = on
        self.level = level
        self.temperature = temperature
    }

    enum State: Int {
        case on = 1
        case off = 0
    }
    
    var onState: State {
        get {
            if let value = on.value?.toValueInt() {
                if let state = State(rawValue: value) {
                    if state == .on && self.service.state == .online {
                        self.service.state = .highlight
                    }
                    return state
                }
            }
            return .off
        }
        set {}
    }
    
    // 设置亮度等级
    func setLevel(percentage: Int) {
        if self.onState == .off {
            if let action = HomeAction(characteristic: self.on, targetValue: State.on.rawValue) {
                _ = self.service.manager?.executeActions(actions: action)
            }
        }
        
        if percentage == 0 {
            if let action = HomeAction(characteristic: self.on, targetValue: State.off.rawValue) {
                _ = self.service.manager?.executeActions(actions: action)
            }
        } else {
            if let action = HomeAction(characteristic: self.level, targetValue: percentage) {
                _ = self.service.manager?.executeActions(actions: action)
            }
        }
    }
    
    var temperatureValue: Int {
        get {
            if let value = temperature.value?.toValueInt() {
                return value
            }
            return 0
        }
    }
    
    func setColorTemperature(temperature: Int) {
        if let action = HomeAction(characteristic: self.temperature, targetValue: temperature) {
            _ = self.service.manager?.executeActions(actions: action)
        }
    }
}

extension ServiceLightColorTemperature: ServiceCellDelegate {
    var icon: UIView {
        var imgSrc = ""
        if let icon = self.service.icon {
            if self.onState == .on && self.state != .offline {
                imgSrc = "service-" + icon + "-on"
            } else {
                imgSrc = "service-" + icon + "-off"
            }
        }
        let imageView = UIImageView(frame: ServiceCellView.iconSize)
        imageView.image = UIImage(named: imgSrc)
        return imageView
    }
    
    var room: String {
        if let r = self.service.room?.name {
            return r
        }
        return "默认房间"
    }
    
    var name: String {
        if self.service.name != "" {
            return self.service.name
        }
        return "请命名"
    }
    
    var state: Service.State {
        return self.service.state
    }
    
    var desc: String {
        if self.service.state == .offline {
            return "离线"
        }
        if self.onState == .on {
            if let value = self.level.value?.toValueInt() {
                return "亮度: \(value)%"
            } else {
                return "开"
            }
        }
        return "关闭"
    }
    
    func toggle() -> HomeAction? {
        var target = State.on
        if self.onState == .on {
            target = .off
        }
        self.service.state = .update
        self.onState = target
        let action = HomeAction(characteristic: self.on, targetValue: target.rawValue)
        return action
    }
}
