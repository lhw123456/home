
//
//  UserLifeStudioController.swift
//  home
//
//  Created by Yun Zeng on 2019/9/29.
//  Copyright © 2019 Yun Zeng. All rights reserved.
//

// Artists of Life
// 生活的艺术家

import UIKit

class UserLifeController: UIViewController {
    
    private lazy var customBar: CustomNavigationBar = {
        let bar = CustomNavigationBar()
        bar.title = "生活研究所"
        bar.subtitle = "成为生活的艺术家"
        bar.icon = UIImage(named: "user-life-studio")
        return bar
    } ()
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(hex: "dedede")
        
        view.addSubview(self.customBar)
        customBar.translatesAutoresizingMaskIntoConstraints = false
        customBar.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        customBar.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        customBar.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        customBar.cancel = {
            self.dismiss(animated: true, completion: nil)
        }
    }
}







